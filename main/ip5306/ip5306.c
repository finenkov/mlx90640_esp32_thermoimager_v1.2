#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "../console/console.h"
#include "driver/gpio.h"
#include "soc/gpio_struct.h"
#include "driver/i2c.h"

#include "ip5306.h"

#define IP5306_PIN_SCL          	22
#define IP5306_PIN_SDA          	21
#define IP5306_I2C_NUM            	I2C_NUM_0        	/*!< I2C port number for master dev */
#define IP5306_I2C_FREQ_HZ        	400000	// 400000   /*!< I2C master clock frequency */
#define IP5306_ADDRESS		0x75	// ip5306 address on i2c bus
#define IP5306_CHARGING_REG 0x70	//
#define IP5306_CHARGED_REG  0x71	//
#define IP5306_BATTERY_REG  0x78	//
#define IP5306_CHARGING_BIT 3		// Is charger attached 
#define IP5306_CHARGED_BIT  3		// Is battery fully charged

#define I2C_MASTER_TX_BUF_DISABLE 0	/*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0	/*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE	/*!< I2C master write */
#define READ_BIT I2C_MASTER_READ	/*!< I2C master read */
#define ACK_CHECK_EN 0x1			/*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0			/*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0					/*!< I2C ack value */
#define NACK_VAL 0x1				/*!< I2C nack value */

void IP5306_I2CInit()
{
	int address = IP5306_ADDRESS;
    int i2c_master_port = IP5306_I2C_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = IP5306_PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = IP5306_PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = IP5306_I2C_FREQ_HZ;
	
    i2c_param_config(i2c_master_port, &conf);
	
    esp_err_t ret = i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
	if (ret == ESP_OK) {
		console_printf(MsgInfo, " IP5306 i2c init OK\n");
	} else {
		console_printf(MsgInfo, " IP5306 i2c init Error\n");
	}
	// For debug
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();	
	i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(i2c_master_port, cmd, 50 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
	if (ret == ESP_OK) {
		console_printf(MsgInfo, " IP5306 found at %x\n", address);
	} else {
		console_printf(MsgInfo, " IP5306 not found at %x\n", address);
	}
}

uint8_t IP5306_isCharging()
{
	int reg = IP5306_Get(IP5306_CHARGING_REG);
	if(reg >> IP5306_CHARGING_BIT & 0x0001)				//To do error handler
		return 1;
	return 0;
}

uint8_t IP5306_isCharged()
{
	int reg = IP5306_Get(IP5306_CHARGED_REG);
	if(reg >> IP5306_CHARGED_BIT & 0x0001)		    	//To do error handler
		return 1;
	return 0;
}

uint8_t IP5306_getBatteryGauge()
{
	int reg = IP5306_Get(IP5306_BATTERY_REG);
	int8_t ret = reg & 0x000000F0;						//To do error handler
	return ret;
}

int IP5306_Get(uint8_t data_addr)
{
	int i2c_master_port = IP5306_I2C_NUM;
	uint8_t chip_addr = IP5306_ADDRESS;
	int reg;
	uint8_t *data = malloc(1);							// ���������� �� heap_caps_malloc
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	
	i2c_master_start(cmd);	
	i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
    i2c_master_start(cmd);		

	i2c_master_write_byte(cmd, chip_addr << 1 | READ_BIT, ACK_CHECK_EN);
	
	i2c_master_read_byte(cmd, data, NACK_VAL);
	i2c_master_stop(cmd);
	
    esp_err_t ret = i2c_master_cmd_begin(i2c_master_port, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
	// For debug
	switch (ret) {
		case ESP_OK:
			reg = data[0];
			break;
		case ESP_ERR_INVALID_ARG:
			printf(" Parameter error\r\n");
			break;
		case ESP_FAIL:
			printf(" Sending command error, slave doesn't ACK the transfer\r\n");
			break;
		case ESP_ERR_INVALID_STATE:
			printf("  I2C driver not installed or not in master mode\r\n");
			break;
		case ESP_ERR_TIMEOUT:
			printf(" Operation timeout because the bus is busy\r\n");
			break;
	}
	//
	if (ret == ESP_OK) {
		reg = data[0];
	} else {
		reg = -1;
	}
	free(data);
	return reg;
}
