#ifndef MAIN_IP5306_IP5306_H_
#define MAIN_IP5306_IP5306_H_

#include <stdio.h>
#include <string.h>
#include "esp_system.h"

#define IP5306_BATTERY_REMAIN_100	0x0000
#define IP5306_BATTERY_REMAIN_75	0x0080
#define IP5306_BATTERY_REMAIN_50	0x00C0
#define IP5306_BATTERY_REMAIN_25	0x00E0
#define IP5306_BATTERY_REMAIN_0		0x00F0
#define IP5306_CHARGING				0x0001
#define IP5306_CHARGED				0x0002

void IP5306_I2CInit();
uint8_t IP5306_isCharging();
uint8_t IP5306_isCharged();
uint8_t IP5306_getBatteryGauge();
int IP5306_Get(uint8_t data_addr);


#endif /* MAIN_IP5306_IP5306_H_ */
